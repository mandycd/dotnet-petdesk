﻿using System;

namespace PetDesk.Castle
{
    public class Injector
    {
        // Fields
        private static IResolver resolver;

        // Methods
        public static T Get<T>()
        {
            return Resolver.Get<T>();
        }

        public static T Get<T>(string key)
        {
            return Resolver.Get<T>(key);
        }

        public static object Get(Type type)
        {
            return Resolver.Get(type);
        }

        public static bool Has(Type type)
        {
            return Resolver.Has(type);
        }

        public static void Release(object instance)
        {
            Resolver.Release(instance);
        }

        // Properties
        public static IResolver Resolver
        {
            get
            {
                return (resolver ?? (resolver = new CastleResolver()));
            }
            set
            {
                resolver = value;
            }
        }
    }
}
