﻿using Castle.MicroKernel.Registration;
using System;

namespace PetDesk.Castle
{
    public interface IResolver
    {
        // Methods
        T Get<T>();
        T Get<T>(string key);
        object Get(Type type);
        bool Has(Type type);
        [Obsolete("IResolver should not depend on Castle Windsor interfaces, as it prevents changing the underlying resolver.")]
        void Install(IWindsorInstaller installer);
        void Release(object instance);
    }
}
