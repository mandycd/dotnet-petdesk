﻿using Castle.Core.Resource;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PetDesk.Castle
{
    public class CastleResolver : IResolver
    {
        // Fields
        private static IWindsorContainer container;
        private static readonly Lazy<IWindsorContainer> lazyContainer = new Lazy<IWindsorContainer>(new Func<IWindsorContainer>(CastleResolver.CreateContainer), LazyThreadSafetyMode.PublicationOnly);

        // Methods
        private static IWindsorContainer CreateContainer()
        {
            IResource resource;
            if (string.IsNullOrEmpty(ConfigFilePath))
            {
                resource = new ConfigResource("castle");
            }
            else
            {
                var uri = new CustomUri("file://" + ConfigFilePath);
                resource = new FileResource(uri);
            }
            return new WindsorContainer(new XmlInterpreter(resource));
        }

        public T Get<T>()
        {
            return Container.Resolve<T>();
        }

        public T Get<T>(string key)
        {
            return Container.Resolve<T>(key);
        }

        public object Get(Type type)
        {
            return Container.Resolve(type);
        }

        public bool Has(Type type)
        {
            return Container.Kernel.HasComponent(type);
        }

        public void Install(IWindsorInstaller installer)
        {
            Container.Install(new IWindsorInstaller[] { installer });
        }

        public void Release(object instance)
        {
            Container.Release(instance);
        }

        // Properties
        public static string ConfigFilePath { get; set; }

        public static IWindsorContainer Container
        {
            get
            {
                return (container ?? lazyContainer.Value);
            }
            set
            {
                container = value;
            }
        }
    }
}
