﻿namespace PetDesk.REST.EndPoints
{
    /// <summary>
    /// A base class which can be used by all the Endpoints classes. 
    /// </summary>
    public abstract class BaseEndPoints
    {
        /// <summary>
        /// Gets or sets the base URL of the REST API.
        /// </summary>
        /// <value>
        /// The base URL.
        /// </value>
        public string BaseUrl { get; set; }
    }
}
