﻿namespace PetDesk.REST.EndPoints
{
    /// <summary>
    /// A class to store Appointment API EndPoints object
    /// </summary>
    /// <seealso cref="PetDesk.REST.EndPoints.BaseEndPoints" />
    public class AppointmentEndPoints : BaseEndPoints
    {
        /// <summary>
        /// Gets or sets the list endpoint of the API.
        /// </summary>
        /// <value>
        /// The list.
        /// </value>
        public string List { get; set; }        
    }
}
