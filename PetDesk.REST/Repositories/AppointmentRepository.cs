﻿using PetDesk.REST.EndPoints;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PetDesk.Contracts;

namespace PetDesk.REST.Repositories
{
    /// <summary>
    /// A repository layer for appointments which does Get and CRUD operations with our REST API.
    /// </summary>
    public class AppointmentRepository : IAppointmentRepository
    {
        /// <summary>
        /// The appointment REST service endpoints
        /// </summary>
        protected AppointmentEndPoints EndPoints;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentRepository"/> class.
        /// </summary>
        /// <param name="endPoints">The end points.</param>
        /// <exception cref="ArgumentNullException">Appointment EndPoints object is null</exception>
        public AppointmentRepository(AppointmentEndPoints endPoints)
        {
            if(endPoints == null)
            {
                throw new ArgumentNullException("Appointment EndPoints object is null");
            }

            this.EndPoints = endPoints;
        }

        /// <summary>
        /// Gets the list of appointments asynchronously.
        /// </summary>
        /// <returns></returns>
        public async Task<Response<List<Appointment>>> ListAsync()
        {
            var appointementsResponse = await RESTService.GetAsync<List<Appointment>>(EndPoints.BaseUrl, EndPoints.List);

            if(false == appointementsResponse.IsSuccess)
            {
                new Response<List<Appointment>>(appointementsResponse.Status, appointementsResponse.Description);
            }                             

            return new Response<List<Appointment>>(appointementsResponse.Content); 
                      
        }
    }
}
