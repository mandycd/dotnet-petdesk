﻿using Newtonsoft.Json;
using PetDesk.Contracts;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PetDesk.REST
{
    public static class RESTService
    {
        /// <summary>
        /// A static async method to get data from a GET endpoint.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static async Task<Response<T>> GetAsync<T>(string baseUrl, string uri)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress =  new Uri(baseUrl);

                httpClient.DefaultRequestHeaders.Clear();

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var res = await httpClient.GetAsync(uri);

                //Check if the response is successful for the request sent.
                if (res.IsSuccessStatusCode)
                {
                    var text = res.Content.ReadAsStringAsync().Result;

                    var result = JsonConvert.DeserializeObject<T>(
                        text
                    );

                    return new Response<T>(result);
                }

                return new Response<T>(res.StatusCode, res.ReasonPhrase);                
            }
        }        
    }
}
