﻿using PetDesk.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetDesk.Services
{
    /// <summary>
    /// A service layer which communicated with our repository layer and also provides some business logic for the application.
    /// </summary>
    public class AppointmentService : IAppointmentService
    {
        private readonly IAppointmentRepository repository;     
        
        protected static Dictionary<int, DateTime> Proposals { get; set; }

        protected static HashSet<int> ConfirmedAppointments { get; set; }

        /// <summary>
        /// Initializes the <see cref="AppointmentService"/> class.
        /// </summary>
        static AppointmentService()
        {
            Proposals = new Dictionary<int, DateTime>();
            ConfirmedAppointments = new HashSet<int>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentService"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <exception cref="ArgumentNullException">Appointment Repositry is null</exception>
        public AppointmentService(IAppointmentRepository repository)
        {
            if(repository == null)
            {
                throw new ArgumentNullException("Appointment Repositry is null");
            }

            this.repository = repository;
        }

        /// <summary>
        /// Gets the list of the requested appointements asynchronous.
        /// </summary>
        /// <returns></returns>
        public async Task<Response<List<Appointment>>> ListRequestedAsync()
        {
            var appointementsResponse = await repository.ListAsync();

            if(false == appointementsResponse.IsSuccess)
            {
                new Response<List<Appointment>>(appointementsResponse.Status, appointementsResponse.Description);
            }

            var appointments = new List<Appointment>();

            //Loop through appointments once to remove any confirmed appointements and also use modified appointments.
            appointementsResponse.Content.ForEach(x =>
            {
                if (ConfirmedAppointments.Contains(x.AppointmentId))
                    return;

                var appointment = x;

                if (Proposals.ContainsKey(x.AppointmentId))
                    appointment.ProposedTime = Proposals[x.AppointmentId];
                
                appointments.Add(appointment);

            });                      

            return new Response<List<Appointment>>(appointments);                       
        }

        /// <summary>
        /// Gets the list of confirmed appointments asynchronous.
        /// </summary>
        /// <returns></returns>
        public async Task<Response<List<Appointment>>> ListConfirmedAsync()
        {
            var appointementsResponse = await repository.ListAsync();

            if (false == appointementsResponse.IsSuccess)
            {
                new Response<List<Appointment>>(appointementsResponse.Status, appointementsResponse.Description);
            }

            var appointments = new List<Appointment>();

            //Lop through appointments once to show only confirmed appointements and also use modified appointments.
            appointementsResponse.Content.ForEach(x =>
            {
                if (ConfirmedAppointments.Contains(x.AppointmentId))
                {                                     
                    appointments.Add(x);
                }                             
            });

            return new Response<List<Appointment>>(appointments);
        }

        /// <summary>
        /// Gets the appointment by Id asynchronously.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Response<Appointment>> GetAsync(int id)
        {
            var appointmentsResponse = await repository.ListAsync();

            if (false == appointmentsResponse.IsSuccess)
            {
                return new Response<Appointment>(appointmentsResponse.Status, appointmentsResponse.Description);
            }

            var appointment = appointmentsResponse.Content.FirstOrDefault(x => x.AppointmentId == id);

            if (null == appointment)
            {
                return new Response<Appointment>(System.Net.HttpStatusCode.NotFound, "The requested appointment either does not exist or is not available for rescheduling.");
            }

            return new Response<Appointment>(appointment);
        }

        /// <summary>
        /// Modifies the appointment asynchronously.
        /// Note : Appointment should have properted UpdatedDate to ensure data integrity. 
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        /// <returns></returns>
        public Task<Response> ModifyAppointmentAsync(Appointment appointment)
        {
            return Task.Run(() =>
            {
                if (Proposals.ContainsKey(appointment.AppointmentId))
                {
                    Proposals[appointment.AppointmentId] = appointment.RequestedDateTime;
                }
                else
                {
                    Proposals.Add(appointment.AppointmentId, appointment.RequestedDateTime);
                }

                return new Response();
            });
        }

        /// <summary>
        /// Confirms the appointment asynchronously.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Task<Response> ConfirmAppointmentAsync(int id)
        {
            return Task.Run(() =>
            {
                var result = ConfirmedAppointments.Add(id);

                if (false == result)
                {
                    return new Response(System.Net.HttpStatusCode.Forbidden, "Appointment already confirmed");
                }

                //If it has been confirmed, then we can remove it from the proposals.
                if (Proposals.ContainsKey(id))
                {
                    Proposals.Remove(id);
                }

                return new Response();
            });
        }
    }
}
