﻿using PetDesk.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PetDesk
{
    /// <summary>
    /// A service layer which communicated with our repository layer and also provides some business logic for the application.
    /// </summary>
    public interface IAppointmentService
    {
        /// <summary>
        /// Gets the list of the requested appointements asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<Response<List<Appointment>>> ListRequestedAsync();

        /// <summary>
        /// Gets the list of confirmed appointments asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<Response<List<Appointment>>> ListConfirmedAsync();

        /// <summary>
        /// Gets the appointment by Id asynchronously.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<Response<Appointment>> GetAsync(int id);

        /// <summary>
        /// Modifies the appointment asynchronously.
        /// Note : Appointment should have properted UpdatedDate to ensure data integrity. 
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        /// <returns></returns>
        Task<Response> ModifyAppointmentAsync(Appointment appointment);

        /// <summary>
        /// Confirms the appointment asynchronously.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<Response> ConfirmAppointmentAsync(int id);
    }
}
