﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PetDesk.Contracts
{
    /// <summary>
    /// A repository layer for appointments. Should be used to do Get and CRUD operations with persistant storage.
    /// </summary>
    public interface IAppointmentRepository
    {
        /// <summary>
        /// Gets the list of appointments asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<Response<List<Appointment>>> ListAsync();
    }
}
