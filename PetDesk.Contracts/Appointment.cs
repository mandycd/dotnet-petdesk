﻿using System;

namespace PetDesk.Contracts
{
    /// <summary>
    /// A composite class for Appointment
    /// </summary>
    public class Appointment
    {
        /// <summary>
        /// Gets or sets the appointment identifier.
        /// </summary>
        /// <value>
        /// The appointment identifier.
        /// </value>
        public int AppointmentId { get; set; }

        /// <summary>
        /// Gets or sets the type of the appointment.
        /// </summary>
        /// <value>
        /// The type of the appointment.
        /// </value>
        public string AppointmentType { get; set; }

        /// <summary>
        /// Gets or sets the create date time.
        /// </summary>
        /// <value>
        /// The create date time.
        /// </value>
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// Gets or sets the requested date time.
        /// </summary>
        /// <value>
        /// The requested date time.
        /// </value>
        public DateTime RequestedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the animal.
        /// </summary>
        /// <value>
        /// The animal.
        /// </value>
        public Animal Animal { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public User User { get; set; }

        /// <summary>
        /// Gets or sets the proposed time.
        /// </summary>
        /// <value>
        /// The proposed time.
        /// </value>
        public DateTime? ProposedTime { get; set; }        
    }
}
