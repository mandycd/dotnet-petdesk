﻿using System;
using System.Net;

namespace PetDesk.Contracts
{
    /// <summary>
    /// A response for an action with no content.
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Response"/> class.
        /// </summary>
        /// <param name="status">The status code.</param>
        /// <param name="description">A description of the response.</param>
        public Response(HttpStatusCode status = HttpStatusCode.OK, string description = null)
        {
            this.Description = description;
            this.Status = status;
        }        

        /// <summary>
        /// Initializes a new instance of the <see cref="Response"/> class from an exception.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public Response(Exception ex)
            : this(HttpStatusCode.InternalServerError, ex.Message)
        {
        }

        /// <summary>
        /// Gets or sets a description of the response, if one is necessary.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets a value indicating whether this response is a success.
        /// </summary>
        public bool IsSuccess
        {
            get { return (int)Status >= 200 && (int)Status < 300; }
        }

        /// <summary>
        /// Gets or sets the status code of the response.
        /// </summary>
        public HttpStatusCode Status { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            return Equals((Response)obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Description != null ? Description.GetHashCode() : 0) * 397) ^ (int)Status;
            }
        }

        private bool Equals(Response other)
        {
            return string.Equals(Description, other.Description) && Status == other.Status;
        }
    }

    /// <summary>
    /// A response for an action with a content object.
    /// </summary>
    public sealed class Response<T> : Response
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Response{T}"/> class.
        /// </summary>
        /// <param name="status">The status code.</param>
        /// <param name="description">A description of the response.</param>
        public Response(HttpStatusCode status = HttpStatusCode.OK, string description = null)
            : base(status, description)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Response{T}"/> class.
        /// </summary>
        /// <param name="content">The response content.</param>
        /// <param name="status">The status code.</param>
        /// <param name="description">A description of the response.</param>
        public Response(T content, HttpStatusCode status = HttpStatusCode.OK, string description = null)
            : base(status, description)
        {
            this.Content = content;
        }
                

        /// <summary>
        /// Initializes a new instance of the <see cref="Response{T}"/> class.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public Response(Exception ex)
            : base(ex)
        {
        }

        /// <summary>
        /// Gets or sets the content of this response.
        /// </summary>
        public T Content { get; set; }
    }
}
