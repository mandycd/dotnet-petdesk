﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetDesk.Contracts
{
    /// <summary>
    /// A class for Animal
    /// </summary>
    public class Animal
    {
        /// <summary>
        /// Gets or sets the animal identifier.
        /// </summary>
        /// <value>
        /// The animal identifier.
        /// </value>
        public int AnimalId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the species.
        /// </summary>
        /// <value>
        /// The species.
        /// </value>
        public string Species { get; set; }

        /// <summary>
        /// Gets or sets the breed.
        /// </summary>
        /// <value>
        /// The breed.
        /// </value>
        public string Breed { get; set; }
    }
}
