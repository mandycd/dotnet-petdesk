﻿using PetDesk.Castle;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PetDesk.Web.Installers
{
    public class InjectorControllerFactory : DefaultControllerFactory
    {
        // Methods
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(0x194, string.Format("The controller for path '{0}' could not be found.", requestContext.HttpContext.Request.Path));
            }
            return (IController)Injector.Get(controllerType);
        }

        public override void ReleaseController(IController controller)
        {
            Injector.Release(controller);
        }
    }
}