﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System.Web.Mvc;

namespace PetDesk.Web.Installers
{
    public class ControllerInstaller : IWindsorInstaller
    {
        private static readonly string[] NAMESPACES = new[]
        {

            "PetDesk.Web",
            "PetDesk.Application"
        };

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            foreach (var ns in NAMESPACES)
            {
                container.Register(Classes.FromAssemblyNamed(ns).BasedOn<IController>().LifestyleTransient());
            }
        }
    }
}