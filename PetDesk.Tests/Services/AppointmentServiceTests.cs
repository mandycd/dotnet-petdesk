﻿using NUnit.Framework;
using PetDesk.Contracts;
using PetDesk.Services;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetDesk.Tests.Services
{
    [TestFixture]
    public class AppointmentServiceTests
    {
        private MockRepository mocks;
        private IAppointmentRepository appointmentRepository;

        private AppointmentService target;

        
        public AppointmentServiceTests()
        {
            mocks = new MockRepository();
            appointmentRepository = mocks.StrictMock<IAppointmentRepository>();
            target = new AppointmentService(appointmentRepository);
        }

        [Test]
        public async Task ListRequestedAsync_ReturnsFromRepository()
        {
            var result = new List<Appointment>() { new Appointment() };
            var wrongResult = new List<Appointment>() { new Appointment() };
            var response = new Response<List<Appointment>>() { Content = result };

            using (mocks.Record())
            {
                Expect.Call(appointmentRepository.ListAsync()).
                    Return(Task.FromResult(response));
            }
            using (mocks.Playback())
            {
                var listResponse = await target.ListRequestedAsync();
                Assert.AreNotEqual(listResponse.Content, wrongResult);
            }
        }

        [Test]
        public async Task ListRequestedAsync_FromRepository()
        {
            var result = new List<Appointment>() { new Appointment() };
            var wrongResult = new List<Appointment>() { new Appointment() };
            var response = new Response<List<Appointment>>() { Content = result };

            using (mocks.Record())
            {
                Expect.Call(appointmentRepository.ListAsync()).
                    Return(Task.FromResult(response));
            }
            using (mocks.Playback())
            {
                var listResponse = await target.ListRequestedAsync();
                Assert.AreEqual(listResponse.Content, result);
            }
        }
    }
}
