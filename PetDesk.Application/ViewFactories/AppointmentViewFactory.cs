﻿using PetDesk.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using PetDesk.Contracts;

namespace PetDesk.Application.ViewFactories
{
    /// <summary>
    /// A viewfactory which is used to create our viewmodels
    /// </summary>
    public class AppointmentViewFactory
    {
        private readonly IAppointmentService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentViewFactory"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public AppointmentViewFactory(IAppointmentService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Gets the requested appointment list view model.
        /// </summary>
        /// <returns></returns>
        public async Task<Response<AppointmentListViewModel>> GetRequestedAppointmentListViewModel()
        {
            var appointmentsResponse = await service.ListRequestedAsync();

            if(false == appointmentsResponse.IsSuccess)
            {
                return new Response<AppointmentListViewModel>(appointmentsResponse.Status, appointmentsResponse.Description);
            }
                       
            var model = new AppointmentListViewModel(appointmentsResponse.Content);

            model.Appointments = model.Appointments.OrderBy(x => x.RequestedDateTime).ToList();

            model.MarkDuplicates();                       

            return new Response<AppointmentListViewModel>(model);
        }

        /// <summary>
        /// Gets the requested appointment list view model.
        /// </summary>
        /// <returns></returns>
        public async Task<Response<AppointmentListViewModel>> GetConfirmedAppointmentListViewModel()
        {
            var appointmentsResponse = await service.ListConfirmedAsync();

            if (false == appointmentsResponse.IsSuccess)
            {
                return new Response<AppointmentListViewModel>(appointmentsResponse.Status, appointmentsResponse.Description);
            }

            var model = new AppointmentListViewModel(appointmentsResponse.Content);

            model.Appointments = model.Appointments.OrderBy(x => x.RequestedDateTime).ToList();                    

            return new Response<AppointmentListViewModel>(model);
        }

        /// <summary>
        /// A method to get EditAppointmentViewModel by provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response<EditAppointmentViewModel>> GetEditAppointmentViewModel(int id)
        {
            var appointmentResponse = await service.GetAsync(id);

            if (false == appointmentResponse.IsSuccess)
            {
                return new Response<EditAppointmentViewModel>(appointmentResponse.Status, appointmentResponse.Description);
            }                        

            return new Response<EditAppointmentViewModel>(new EditAppointmentViewModel(appointmentResponse.Content));
        }
    }
}
