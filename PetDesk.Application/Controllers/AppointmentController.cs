﻿using PetDesk.Application;
using PetDesk.Application.Contracts;
using PetDesk.Application.ViewFactories;
using PetDesk.Application.ViewModels;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PetDesk.Application.Controllers
{
    [RoutePrefix("Appointment")]
    public class AppointmentController : Controller
    {
        private readonly AppointmentViewFactory appointmentViewFactory;
        private readonly IAppointmentDomainFacade domainFacade;

        /// <summary>
        /// Home Controller, We ae using it for displaying list of Appointment Requests
        /// </summary>
        public AppointmentController(AppointmentViewFactory appointmentViewFactory, IAppointmentDomainFacade domainFacade)
        {
            this.appointmentViewFactory = appointmentViewFactory;
            this.domainFacade = domainFacade;
        }

        public ActionResult Index()
        {
            //For now just redirect to list page.
            return RedirectToAction("RequestedList");
        }

        /// <summary>
        /// Action method for Index page which displays the list of Appointments.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> RequestedList()
        {
            var modelResponse = await appointmentViewFactory.GetRequestedAppointmentListViewModel();

            ViewBag.Message = TempData["Message"];

            if (false == modelResponse.IsSuccess)
            {
                ViewBag.Message = modelResponse.Description;
            }

            return View(modelResponse.Content ?? new Application.ViewModels.AppointmentListViewModel());
        }

        /// <summary>
        /// Action method for Index page which displays the list of Appointments.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ConfirmedList()
        {
            var modelResponse = await appointmentViewFactory.GetConfirmedAppointmentListViewModel();

            ViewBag.Message = TempData["Message"];

            if (false == modelResponse.IsSuccess)
            {
                ViewBag.Message = modelResponse.Description;
            }

            return View(modelResponse.Content ?? new Application.ViewModels.AppointmentListViewModel());
        }

        /// <summary>
        /// Action method for confirmation of an appointment. 
        /// We should ideally have a confirmation alert before we hit this endpoint
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Confirm/{id}")]
        public async Task<ActionResult> Confirm(int id)
        {
            var result = await domainFacade.ConfirmAppointmentAsync(id);

            TempData["Message"] = result.IsSuccess ? "Appointment confirmed" : result.Description;

            return RedirectToAction("RequestedList");
        }

        /// <summary>
        /// An action method to display a form for Reschedule of Appointment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Reschedule/{id}")]
        public async Task<ActionResult> Reschedule(int id)
        {
            var modelResponse = await appointmentViewFactory.GetEditAppointmentViewModel(id);

            ViewBag.Message = TempData["Message"];

            if (false == modelResponse.IsSuccess)
            {
                ViewBag.Message = modelResponse.Description;
            }

            return View(modelResponse.Content ?? new EditAppointmentViewModel());
        }

        /// <summary>
        /// A Post action method which accepts a request to reschedule an appointment.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Reschedule/{id}")]
        public async Task<ActionResult> Reschedule(int id, EditAppointmentViewModel model)
        {
            if (false == ModelState.IsValid)
            {
                return View(model);
            }

            if (model.RequestedDateTime < DateTime.Now)
            {
                ViewBag.Message = "You can only schedule for future date";
                return View(model);
            }

            var result = await domainFacade.ModifyAppointmentAsync(id, model);

            TempData["Message"] = result.IsSuccess ? "Appointment Rescheduled" : result.Description;

            return RedirectToAction("RequestedList");
        }
    }
}
