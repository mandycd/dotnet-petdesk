﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PetDesk.Application.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace PetDesk.Application.Installers
{
    /// <summary>
    /// Default castle installer for PetDesk.Application library.
    /// </summary>
    /// <seealso cref="Castle.MicroKernel.Registration.IWindsorInstaller" />
    public class DefaultInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                    .InNamespace("PetDesk.Application.ViewFactories")
                    .WithServiceSelf(),

                   Component.For<IAppointmentDomainFacade>().ImplementedBy<DomainFacade>().Named("DomainFacade"));
        }
    }
}
