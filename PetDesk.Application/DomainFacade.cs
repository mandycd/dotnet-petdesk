﻿using PetDesk.Application.Contracts;
using PetDesk.Application.ViewModels;
using PetDesk.Contracts;
using System.Threading.Tasks;

namespace PetDesk.Application
{
    /// <summary>
    /// Domain layer to send requests from UI to our persistant storage.
    /// </summary>
    public class DomainFacade : IAppointmentDomainFacade
    {
        private readonly IAppointmentService appointmentService;

        public DomainFacade(IAppointmentService appointmentService)
        {
            this.appointmentService = appointmentService;
        }

        public async Task<Response> ConfirmAppointmentAsync(int id)
        {
            return await appointmentService.ConfirmAppointmentAsync(id);
        }

        public async Task<Response> ModifyAppointmentAsync(int id, EditAppointmentViewModel model)
        {
            var appointmentResponse = await appointmentService.GetAsync(id);

            if (false == appointmentResponse.IsSuccess)
            {
                return new Response(appointmentResponse.Status, appointmentResponse.Description);
            }

            if(appointmentResponse.Content.RequestedDateTime.Equals(model.RequestedDateTime))
            {
                return new Response();
            }

            var updatedAppointment = model.UpdateSchedule(appointmentResponse.Content);

            var appointmentModifyResponse = await appointmentService.ModifyAppointmentAsync(updatedAppointment);

            return appointmentModifyResponse;
        }
    }
}
