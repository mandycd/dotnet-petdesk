﻿using PetDesk.Contracts;
using System;
using System.ComponentModel.DataAnnotations;

namespace PetDesk.Application.ViewModels
{
    /// <summary>
    /// A class to store the Edit Appointment Viewmodel
    /// </summary>
    public class EditAppointmentViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditAppointmentViewModel"/> class.
        /// </summary>
        public EditAppointmentViewModel()
        {
            User = new UserViewModel();
            Animal = new AnimalViewModel();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditAppointmentViewModel"/> class.
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        public EditAppointmentViewModel(Appointment appointment)
        {
            Id = appointment.AppointmentId;
            AppointmentType = appointment.AppointmentType;
            CreateDateTime = appointment.CreateDateTime;
            RequestedDateTime = appointment.RequestedDateTime;

            if (appointment.User != null)
            {
                User = new UserViewModel(appointment.User);
            }

            if (appointment.Animal != null)
            {
                Animal = new AnimalViewModel(appointment.Animal);
            }
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the type of the appointment.
        /// </summary>
        /// <value>
        /// The type of the appointment.
        /// </value>
        public string AppointmentType { get; set; }

        /// <summary>
        /// Gets or sets the create date time.
        /// </summary>
        /// <value>
        /// The create date time.
        /// </value>
        [Display(Name = "Created On")]
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// Gets or sets the requested date time.
        /// </summary>
        /// <value>
        /// The requested date time.
        /// </value>
        [Display(Name = "Requested On")]
        public DateTime RequestedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public UserViewModel User { get; set; }

        /// <summary>
        /// Gets or sets the animal.
        /// </summary>
        /// <value>
        /// The animal.
        /// </value>
        public AnimalViewModel Animal { get; set; }

        /// <summary>
        /// Updates the Requested DateTime of the provided appointment with RequestedDateTime of this model..
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        /// <returns></returns>
        public Appointment UpdateSchedule(Appointment appointment)
        {
            appointment.RequestedDateTime = RequestedDateTime;
            return appointment;
        }
    }
}
