﻿using PetDesk.Contracts;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// 
/// </summary>
namespace PetDesk.Application.ViewModels
{
    /// <summary>
    /// A viewmodel for storing Appointment List data.
    /// </summary>
    public class AppointmentListViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentListViewModel"/> class.
        /// </summary>
        public AppointmentListViewModel()
        {
            Appointments = new List<AppointmentViewModel>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentListViewModel"/> class.
        /// </summary>
        /// <param name="appointments">The appointments.</param>
        public AppointmentListViewModel(List<Appointment> appointments)
        {
            Appointments = appointments.Select(x=> new AppointmentViewModel(x)).ToList();
        }

        /// <summary>
        /// Gets or sets the appointments.
        /// </summary>
        /// <value>
        /// The appointments.
        /// </value>
        public List<AppointmentViewModel> Appointments { get; set; }

        /// <summary>
        /// Find & mark duplicate requested dates.
        /// </summary>
        public void MarkDuplicates()
        {
            //Add an extra bit of logic to find duplicates 
            var duplicateAppointments = Appointments.GroupBy(x => x.RequestedDateTime).Where(x => x.Count() > 1)
                .SelectMany(x => x.ToList().Select(y => y.Id));

            if (duplicateAppointments.Any())
            {
                foreach (var appointment in Appointments.Where(x => duplicateAppointments.Contains(x.Id)))
                {
                    appointment.HasDuplicate = true;
                }
            }
        }
    }    
}
