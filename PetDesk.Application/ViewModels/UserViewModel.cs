﻿using PetDesk.Contracts;
using System.ComponentModel.DataAnnotations;

namespace PetDesk.Application.ViewModels
{
    /// <summary>
    /// A viewmodel class to hold data for user.
    /// </summary>
    public class UserViewModel
    {
        /// <summary>
        /// Default constructor, needed to create a new instance.
        /// </summary>
        public UserViewModel()
        {
        }

        /// <summary>
        /// Constructor which accepts a model and sets properties of this viewmodel.
        /// </summary>
        /// <param name="user">The user model</param>
        public UserViewModel(User user)
        {
            Id = user.UserId;
            FirstName = user.FirstName;
            LastName = user.LastName;
        }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>       
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets the full name of the user for Display purposes.
        /// </summary>
        /// <returns>A user model.</returns>
        [Display(Name = "Full Name")]
        public string FullName
        {
            get
            {
                return string.Concat(FirstName ?? "", " ", LastName ?? "");
            }
        }
    }
}
