﻿using PetDesk.Contracts;
using System.ComponentModel.DataAnnotations;

namespace PetDesk.Application.ViewModels
{
    /// <summary>
    /// A viewmodel for Animal data. 
    /// </summary>
    public class AnimalViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AnimalViewModel"/> class.
        /// </summary>
        public AnimalViewModel()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimalViewModel"/> class.
        /// </summary>
        /// <param name="animal">The animal.</param>
        public AnimalViewModel(Animal animal)
        {
            Id = animal.AnimalId;
            FirstName = animal.FirstName;
            Species = animal.Species;
            Breed = animal.Breed;
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [Display(Name = "Name")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the species.
        /// </summary>
        /// <value>
        /// The species.
        /// </value>
        public string Species { get; set; }

        /// <summary>
        /// Gets or sets the breed.
        /// </summary>
        /// <value>
        /// The breed.
        /// </value>
        public string Breed { get; set; }
    }
}
