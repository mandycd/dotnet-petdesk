﻿using PetDesk.Contracts;
using System;

namespace PetDesk.Application.ViewModels
{
    /// <summary>
    /// A class for Appointment viewmodel.
    /// </summary>
    public class AppointmentViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentViewModel"/> class.
        /// </summary>
        public AppointmentViewModel()
        {
            User = new UserViewModel();
            Animal = new AnimalViewModel();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentViewModel"/> class.
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        public AppointmentViewModel(Appointment  appointment)
        {
            Id = appointment.AppointmentId;
            AppointmentType = appointment.AppointmentType;
            CreateDateTime = appointment.CreateDateTime;
            RequestedDateTime = appointment.RequestedDateTime;
            ProposedTime = appointment.ProposedTime;

            if (appointment.User != null)
            {
                User = new UserViewModel(appointment.User);
            }

            if (appointment.Animal != null)
            {
                Animal = new AnimalViewModel(appointment.Animal);
            }
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the type of the appointment.
        /// </summary>
        /// <value>
        /// The type of the appointment.
        /// </value>
        public string AppointmentType { get; set; }

        /// <summary>
        /// Gets or sets the create date time.
        /// </summary>
        /// <value>
        /// The create date time.
        /// </value>
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// Gets or sets the requested date time.
        /// </summary>
        /// <value>
        /// The requested date time.
        /// </value>
        public DateTime RequestedDateTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has duplicate.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has duplicate; otherwise, <c>false</c>.
        /// </value>
        public bool HasDuplicate { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public UserViewModel User { get; set; }

        /// <summary>
        /// Gets or sets the animal.
        /// </summary>
        /// <value>
        /// The animal.
        /// </value>
        public AnimalViewModel Animal { get; set; }

        /// <summary>
        /// Gets or sets the proposed time.
        /// </summary>
        /// <value>
        /// The proposed time.
        /// </value>
        public DateTime? ProposedTime { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has proposed time.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has proposed time; otherwise, <c>false</c>.
        /// </value>
        public string ProposedTimeString
        {
            get
            {
                if(ProposedTime.HasValue && ProposedTime > DateTime.Now)
                {
                    return string.Concat(ProposedTime.Value.ToLongDateString(), " ", ProposedTime.Value.ToShortTimeString());
                }

                return string.Empty;
            }
        }
    }
}
