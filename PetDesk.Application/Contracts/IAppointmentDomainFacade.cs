﻿using PetDesk.Application.ViewModels;
using PetDesk.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PetDesk.Application.Contracts
{
    public interface IAppointmentDomainFacade
    {
        Task<Response> ModifyAppointmentAsync(int id, EditAppointmentViewModel model);

        Task<Response> ConfirmAppointmentAsync(int id);
    }
}
